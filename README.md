# Recommendation service

## Installation steps

1. Install `git` package with `apt-get install`
2. clone this repository with `git clone`
3. You need `Python 3.10` and `pip` to be installed (**check** if they are already installed first)
4. Install the packages of the `requirements.txt` file with `pip`
5. Generate the gRPC files by using the Protocol Buffers compiler `protoc` (use the Python module, not the bash command)
6. Launch the file `recommendation_server.py`. Add the environment variable `DISABLE_PROFILER=1` to disable the profiler (which degrades the service)

Note : `recommendation`depends on the service `catalog`, you have to respect this order! So do not start with `recommendation`!
